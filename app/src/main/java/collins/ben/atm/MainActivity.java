package collins.ben.atm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import collins.ben.atm.list.ConsoleListAdapter;
import collins.ben.atm.list.RecyclerListAdapter;
import collins.ben.atm.manager.BillManager;
import collins.ben.atm.manager.MyParseManager;
import collins.ben.atm.model.Command;
import collins.ben.atm.model.ConsoleOutput;
import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity implements TextView.OnEditorActionListener {

    @Bind(R.id.bsToolbar)
    Toolbar toolbar;
    @Bind(R.id.bsToolbarTitle)
    TextView toolbarTitle;
    @Bind(R.id.etCommandInput)
    EditText commandEditText;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    private RecyclerListAdapter listAdapter;
    private List<ConsoleOutput> outputs;
    private BillManager billManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        billManager = new BillManager();
        setSupportActionBar(toolbar);
        toolbarTitle.setText(R.string.app_name);
        commandEditText.setOnEditorActionListener(this);
        initList();
        commandEditText.requestFocus();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Util.dismissKeyboard(commandEditText);
        EventBus.getDefault().unregister(this);
    }

    private void initList() {
        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        outputs = new ArrayList<>();
        listAdapter = new ConsoleListAdapter(outputs);
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        String cmdLine = v.getText().toString().trim();
        commandEditText.setText("");
        addConsoleOutput(new ConsoleOutput(cmdLine, true));
        //ParseManager will broadcast any event to onEvent(Command command)
        MyParseManager.getInstance().parse(cmdLine);
        return true;
    }

    private void addConsoleOutput(ConsoleOutput consoleOutput) {
        outputs.add(consoleOutput);
        listAdapter.notifyItemInserted(outputs.size() - 1);
        recyclerView.scrollToPosition(outputs.size() - 1);
    }

    @SuppressWarnings("unused")
    public void onEvent(Command command) {
        switch (command.getCommandType()) {
            case QUIT:
                this.finish();
                break;
            case RESTOCK:
                billManager.restockBills();
                displayMachineBalance("");
                break;
            case GET_COUNT:
                Integer[] values = MyParseManager.getValuesFromArgs(command.getArgs());
                if (values != null) {
                    addConsoleOutput(
                            new ConsoleOutput(billManager.getDenominations(values), false));
                } else {
                    addConsoleOutput(
                            new ConsoleOutput(getString(R.string.failure_invalid), false));
                }
                break;
            case WITHDRAW:
                if (command.getArgs() != null && command.getArgs().length > 0) {
                    int value = MyParseManager.getValueFromArg(command.getArgs()[0]);
                    if (value > -1) {
                        if (billManager.performWithdraw(value)) {
                            displayMachineBalance(
                                    getString(R.string.withdraw_success_format, value));
                        } else {
                            addConsoleOutput(new ConsoleOutput(
                                    getString(R.string.failure_insufficient_funds), false));
                        }
                    } else {
                        addConsoleOutput(
                                new ConsoleOutput(getString(R.string.failure_invalid),
                                        false));
                    }
                } else {
                    addConsoleOutput(
                            new ConsoleOutput(getString(R.string.failure_invalid),
                                    false));
                }
                break;
            case INVALID:
                addConsoleOutput(
                        new ConsoleOutput(getString(R.string.failure_invalid), false));
                break;

        }

    }


    private void displayMachineBalance(String append) {
        addConsoleOutput(
                new ConsoleOutput(append + "Machine Balance:\n" +
                        billManager.getDenomincations(), false));
    }

}
