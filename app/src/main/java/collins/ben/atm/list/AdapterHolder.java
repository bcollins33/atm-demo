package collins.ben.atm.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import de.greenrobot.event.EventBus;

/**
 * Interface for the view holder pattern used in the Custom list view adapters.
 *
 * @param <T> the object being adapted to a listview
 * @author bcollins
 */
public abstract class AdapterHolder<T> extends RecyclerView.ViewHolder {

    protected View mainView;

    public AdapterHolder(View view) {
        super(view);
        mainView = view;
    }


    /**
     * Fills a given view with information from the holders data object
     *
     * @param t        - data to fill view with
     * @param position - the position of the view in the list
     */
    public abstract void populateView(T t, final int position);

    public View getMainView() {
        return mainView;
    }


    public void setupMainPress(final T t, final int position, final int eventId) {
        mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(
                        new AdapterItemClickedEvent(t, position, AdapterHolder.this,
                                eventId));
            }
        });
    }

    public void removeMainPress() {
        mainView.setOnClickListener(null);
    }


}