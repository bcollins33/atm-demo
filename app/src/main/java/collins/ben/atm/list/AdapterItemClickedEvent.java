package collins.ben.atm.list;


public class AdapterItemClickedEvent {

    private Object object;
    private int pos;
    private AdapterHolder holder;
    private int eventId;


    public AdapterItemClickedEvent(Object object, int pos, AdapterHolder holder, int eventId) {
        this.holder = holder;
        this.object = object;
        this.pos = pos;
        this.eventId = eventId;

    }

    public int getEventId() {
        return eventId;
    }

    public AdapterHolder getHolder() {
        return holder;
    }

    public Object getObject() {
        return object;
    }

    public int getPos() {
        return pos;
    }
}
