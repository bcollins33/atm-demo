package collins.ben.atm.list;

import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import collins.ben.atm.R;
import collins.ben.atm.model.ConsoleOutput;

/**
 * Created by mamba on 4/11/2016.
 */
public class ConsoleAdapterHolder extends AdapterHolder<ConsoleOutput> {

    @Bind(R.id.tvConsoleText)
    TextView text;
    @Bind(R.id.containerLeft)
    View containerLeft;
    @Bind(R.id.containerRight)
    View containerRight;

    public ConsoleAdapterHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void populateView(ConsoleOutput output, int position) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)
                text.getLayoutParams();

        if (output.isUserInput()) {
            containerLeft.setVisibility(View.VISIBLE);
            containerRight.setVisibility(View.GONE);
            params.gravity = Gravity.RIGHT;
        } else {
            containerLeft.setVisibility(View.GONE);
            containerRight.setVisibility(View.VISIBLE);
            params.gravity = Gravity.LEFT;
        }
        text.setLayoutParams(params);
        text.setText(output.getCommand());
    }
}
