package collins.ben.atm.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import collins.ben.atm.R;
import collins.ben.atm.model.ConsoleOutput;

/**
 * Created by mamba on 4/11/2016.
 */
public class ConsoleListAdapter extends RecyclerListAdapter<ConsoleOutput> {

    public ConsoleListAdapter(List<ConsoleOutput> dataset) {
        super(dataset);
    }

    @Override
    public AdapterHolder<ConsoleOutput> getAdapterHolder(LayoutInflater inflater,
                                                         ViewGroup parent) {
        View view = inflater.inflate(R.layout.listitem_console_output, parent, false);
        return new ConsoleAdapterHolder(view);
    }

}
