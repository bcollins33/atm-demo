package collins.ben.atm.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;


public abstract class RecyclerListAdapter<T> extends RecyclerView.Adapter<AdapterHolder<T>> {


    protected List<T> mDataset;

    public RecyclerListAdapter(List<T> dataset) {
        this.mDataset = dataset;
    }

    public RecyclerListAdapter(){

    }

    public List<T> getmDataset() {
        return mDataset;
    }

    public void setmDataset(List<T> mDataset) {
        this.mDataset = mDataset;
    }

    public void add(int position, T item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(T item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public AdapterHolder<T> onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AdapterHolder<T> holder = getAdapterHolder(inflater, parent);
        return holder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AdapterHolder<T> holder, int position) {
        holder.populateView(mDataset.get(position), position);
    }

    public abstract AdapterHolder<T> getAdapterHolder(LayoutInflater inflater, ViewGroup parent);

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}