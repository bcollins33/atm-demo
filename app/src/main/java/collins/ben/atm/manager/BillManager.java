package collins.ben.atm.manager;

import java.util.ArrayList;
import java.util.List;


public class BillManager {

    private static final int RESTOCK_COUNT = 10;
    private static final int[] RESTOCK_BILL_VALUES = {100, 50, 20, 10, 5, 1};

    private Integer[] billValues;
    private Integer[] billAmounts;

    public BillManager() {
        restockBills();
    }

    public void restockBills() {
        billValues = new Integer[RESTOCK_BILL_VALUES.length];
        billAmounts = new Integer[RESTOCK_BILL_VALUES.length];
        for (int i = 0; i < RESTOCK_BILL_VALUES.length; i++) {
            billAmounts[i] = RESTOCK_COUNT;
            billValues[i] = RESTOCK_BILL_VALUES[i];
        }
    }

    public boolean performWithdraw(int value) {
        //get all possible solutions
        List<Integer[]> solutions = solutions(billValues, billAmounts,
                new int[RESTOCK_BILL_VALUES.length], value, 0);
        if (solutions.size() == 0) {
            return false;
        }

        //choose most efficient
        //lets choose the fist index with low bills
        int min = Integer.MAX_VALUE;
        int minIndex = -1;
        for (int i = 0; i < solutions.size(); i++) {
            int count = numberOfBills(solutions.get(i));
            if (count < min) {
                min = count;
                minIndex = i;
            }
        }
        Integer[] theSolution = solutions.get(minIndex);
        for (int i = 0; i < theSolution.length; i++) {
            billAmounts[i] = billAmounts[i] - theSolution[i];
        }
        return true;
    }

    private int numberOfBills(Integer[] variation) {
        int count = 0;
        for (int i : variation) {
            count = count + i;
        }
        return count;
    }

    public List<Integer[]> getTestSolutions(int value) {
        return solutions(billValues, billAmounts, new int[RESTOCK_BILL_VALUES.length], value, 0);
    }

    public static String getPrintOfVariation(Integer[] variation) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < variation.length; i++) {
            builder.append(String.format("%d(%d)", RESTOCK_BILL_VALUES[i], variation[i]));
            if (i != variation.length - 1)
                builder.append(", ");
        }
        builder.append("]");
        return builder.toString();
    }

    /**
     * A Nice solution for getting all possible solutions
     * http://stackoverflow.com/questions/22128759/atm-algorithm-of-giving-money-with-limited-amount-of-bank-notes
     */
    private List<Integer[]> solutions(Integer[] values, Integer[] amounts, int[] variation,
                                      int price, int position) {
        List<Integer[]> list = new ArrayList<>();
        int value = compute(values, variation);
        if (value < price) {
            for (int i = position; i < values.length; i++) {
                if (amounts[i] > variation[i]) {
                    int[] newVariation = variation.clone();
                    newVariation[i]++;
                    List<Integer[]> newList = solutions(values, amounts, newVariation, price, i);
                    if (newList != null) {
                        list.addAll(newList);
                    }
                }
            }
        } else if (value == price) {
            list.add(myCopy(variation));
        }
        return list;
    }

    private int compute(Integer[] values, int[] variation) {
        int ret = 0;
        for (int i = 0; i < variation.length; i++) {
            ret += values[i] * variation[i];
        }
        return ret;
    }

    private Integer[] myCopy(int[] ar) {
        Integer[] ret = new Integer[ar.length];
        for (int i = 0; i < ar.length; i++) {
            ret[i] = ar[i];
        }
        return ret;
    }

    public Integer[] getBillValues() {
        return billValues;
    }

    public Integer[] getBillAmounts() {
        return billAmounts;
    }

    public String getDenomincations() {
        return getDenominations(billValues);
    }

    public String getDenominations(Integer... values) {
        StringBuilder builder = new StringBuilder();
        boolean valueAdded = false;
        for (int i = 0; i < values.length; i++) {
            if (valueAdded) {
                builder.append("\n");
            }
            int index = -1;
            for (int j = 0; j < billValues.length; j++) {
                if (billValues[j] == values[i]) {
                    index = j;
                    break;
                }
            }
            if (index > -1) {
                valueAdded = true;
                builder.append(String.format("$%d - %d",
                        billValues[index], billAmounts[index]));
            }
        }
        return builder.toString();
    }


}
