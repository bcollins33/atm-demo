package collins.ben.atm.manager;

import android.text.TextUtils;

import collins.ben.atm.model.Command;
import collins.ben.atm.model.command.GetCountsCommandOption;
import collins.ben.atm.model.command.QuitCommandOption;
import collins.ben.atm.model.command.RestockCommandOption;
import collins.ben.atm.model.command.WithdrawCommandOption;
import de.greenrobot.event.EventBus;


public class MyParseManager {

    private static MyParseManager inst;
    private Parser parser;

    public static MyParseManager getInstance() {
        synchronized (MyParseManager.class) {
            if (inst == null) {
                inst = new MyParseManager();
            }
            return inst;
        }
    }

    private MyParseManager() {
        parser = new Parser();
        parser.addOptions(new WithdrawCommandOption());
        parser.addOptions(new QuitCommandOption());
        parser.addOptions(new GetCountsCommandOption());
        parser.addOptions(new RestockCommandOption());
    }


    public void parse(String line) {
        if (!parser.parse(line)) {
            EventBus.getDefault().post(new Command(Command.CommandType.INVALID));
        }
    }

    public static Integer[] getValuesFromArgs(String[] args) {
        if (args == null || args.length == 0) {
            return null;
        }
        boolean success = true;
        Integer[] values = new Integer[args.length];
        for (int i = 0; i < args.length; i++) {
            int value = getValueFromArg(args[i]);
            if (value < 0) {
                success = false;
                break;
            }
            values[i] = value;
        }
        if (success) {
            return values;
        }
        return null;
    }

    public static int getValueFromArg(String arg) {
        int value = -1;
        if (TextUtils.isEmpty(arg)) {
            return value;
        }
        if (arg.substring(0, 1).equalsIgnoreCase("$")) {
            String stringValue = arg.substring(1, arg.length());
            try {
                value = Integer.parseInt(stringValue);
            } catch (NumberFormatException ex) {

            }
        }
        return value;

    }
}
