package collins.ben.atm.manager;

import java.util.HashMap;

import collins.ben.atm.model.CommandOptions;


public class Parser {

    private HashMap<String, CommandOptions> options = new HashMap<>();

    public void addOptions(CommandOptions option) {
        options.put(option.getCommand(), option);
    }

    public boolean parse(String line) {
        // a better parser here would handle quoted strings
        String[] split = line.split("\\s");

        String cmd = split[0];
        String[] args = new String[split.length - 1];
        if (args.length > 0)
            System.arraycopy(split, 1, args, 0, args.length);

        CommandOptions opts = options.get(cmd);
        if (opts == null) {
            return false;
        } else {
            opts.parse(args);
            return true;
        }
    }
}
