package collins.ben.atm.model;


public class Bill {

    private int value;
    private int availableCount;

    public Bill(int value, int availableCount) {
        this.value = value;
        this.availableCount = availableCount;
    }

    public int getValue() {
        return value;
    }

    public int getAvailableCount() {
        return availableCount;
    }
}
