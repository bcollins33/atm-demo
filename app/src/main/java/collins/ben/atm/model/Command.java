package collins.ben.atm.model;


public class Command {

    public enum CommandType {WITHDRAW, RESTOCK, GET_COUNT, QUIT, INVALID}

    private String[] args;
    private CommandType commandType;

    public Command(CommandType type) {
        commandType = type;
    }

    public Command(CommandType type, String[] args) {
        commandType = type;
        this.args = args;
    }

    public String[] getArgs() {
        return args;
    }

    public CommandType getCommandType() {
        return commandType;
    }
}
