package collins.ben.atm.model;


public class CommandOptions {

    private String command;
    private boolean hasArguments;

    public CommandOptions(String command, boolean hasArguments) {
        this.command = command;
        this.hasArguments = hasArguments;
    }

    public void parse(String[] args) {
        StringBuilder builder = new StringBuilder(String.format("Received Command %s", command));
        builder.append(" ");
        if (hasArguments) {
            if (args != null && args.length > 0) {
                for (String arg : args) {
                    builder.append(arg);
                    builder.append(" ");
                }
            }
        }
        System.out.println(builder.toString());
    }

    public String getCommand() {
        return command;
    }
}
