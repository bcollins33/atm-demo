package collins.ben.atm.model;

/**
 * Created by mamba on 4/11/2016.
 */
public class ConsoleOutput {

    private String command;
    private boolean userInput;

    public ConsoleOutput(String command, boolean userInput) {
        this.command = command;
        this.userInput = userInput;
    }

    public String getCommand() {
        return command;
    }

    public boolean isUserInput() {
        return userInput;
    }
}
