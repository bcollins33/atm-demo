package collins.ben.atm.model.command;

import collins.ben.atm.model.Command;
import collins.ben.atm.model.CommandOptions;
import de.greenrobot.event.EventBus;

/**
 * Created by mamba on 4/11/2016.
 */
public abstract class EventBusCommandOption extends CommandOptions {

    public EventBusCommandOption(String command, boolean hasArguments) {
        super(command, hasArguments);
    }

    @Override
    public void parse(String[] args) {
        EventBus.getDefault().post(getCommandToBroadcast(args));
    }

    abstract Command getCommandToBroadcast(String[] args);
}
