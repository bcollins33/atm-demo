package collins.ben.atm.model.command;

import collins.ben.atm.model.Command;

/**
 * Created by mamba on 4/11/2016.
 */
public class GetCountsCommandOption extends EventBusCommandOption {

    public GetCountsCommandOption() {
        super("I", true);
    }

    @Override
    Command getCommandToBroadcast(String[] args) {
        return new Command(Command.CommandType.GET_COUNT, args);
    }
}
