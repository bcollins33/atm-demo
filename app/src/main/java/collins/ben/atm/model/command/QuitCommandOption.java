package collins.ben.atm.model.command;

import collins.ben.atm.model.Command;

/**
 * Created by mamba on 4/11/2016.
 */
public class QuitCommandOption extends EventBusCommandOption {

    public QuitCommandOption() {
        super("Q", false);
    }

    @Override
    Command getCommandToBroadcast(String[] args) {
        return new Command(Command.CommandType.QUIT);
    }
}
