package collins.ben.atm.model.command;

import collins.ben.atm.model.Command;

/**
 * Created by mamba on 4/11/2016.
 */
public class WithdrawCommandOption extends EventBusCommandOption {

    public WithdrawCommandOption() {
        super("W", true);
    }

    @Override
    Command getCommandToBroadcast(String[] args) {
        return new Command(Command.CommandType.WITHDRAW, args);
    }
}
