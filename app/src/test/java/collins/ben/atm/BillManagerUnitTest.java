package collins.ben.atm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import collins.ben.atm.manager.BillManager;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class BillManagerUnitTest {

    private BillManager billManager;

    @Before
    public void setUp() {
        billManager = new BillManager();
    }

    @After
    public void tearDown() {
        System.out.println("");
    }


    /**
     * After attempting to withdraw 1, app didnt broadcast an event.
     */
    @Test
    public void catchWithdraw1Bug() throws Exception {
        assertTrue(billManager.performWithdraw(1));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
    }

    @Test
    public void checkBasicBillWithdraw() throws Exception {
        assertTrue(billManager.performWithdraw(208));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
        assertTrue(billManager.performWithdraw(9));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
        assertFalse(billManager.performWithdraw(9));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
    }

    @Test
    public void checkBasicBillWithdrawAndRestock() throws Exception {
        assertTrue(billManager.performWithdraw(208));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
        assertTrue(billManager.performWithdraw(9));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
        assertFalse(billManager.performWithdraw(9));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));

        System.out.println("Restocking bills....");
        billManager.restockBills();
        assertTrue(billManager.performWithdraw(9));
        System.out.println(BillManager.getPrintOfVariation(billManager.getBillAmounts()));
    }

    @Test
    public void testDenominations() throws Exception {
        assertTrue(billManager.performWithdraw(208));
        System.out.println(billManager.getDenominations(1, 5));
    }


    @Test
    public void testAllPossibilities() {
        int maxNumber = 0;
        for (int bill : billManager.getBillAmounts()) {
            maxNumber += (bill * 10);
        }

        for (int i = 0; i < maxNumber; i++) {
            assertTrue(billManager.performWithdraw(i));
            billManager.restockBills();
        }
    }

}