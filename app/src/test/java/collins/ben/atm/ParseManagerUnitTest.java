package collins.ben.atm;

import org.junit.Test;

import collins.ben.atm.manager.MyParseManager;
import collins.ben.atm.manager.Parser;
import collins.ben.atm.model.CommandOptions;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class ParseManagerUnitTest {


    @Test
    public void testParseWithdraw() {
        Parser parser = new Parser();
        parser.addOptions(new CommandOptions("R", false));
        parser.addOptions(new CommandOptions("W", true));
        parser.addOptions(new CommandOptions("I", true));
        parser.addOptions(new CommandOptions("Q", false));

        assertTrue(parser.parse("W $145"));
        assertTrue(parser.parse("I $120 $1 $5"));
        assertTrue(parser.parse("Q"));
        assertTrue(parser.parse("R"));

        assertFalse(parser.parse("K"));
    }

    @Test
    public void testParsingArgs() {
        int value = MyParseManager.getValueFromArg("$100");
        assertEquals(100, value);

        value = MyParseManager.getValueFromArg("$155");
        assertEquals(155, value);

        value = MyParseManager.getValueFromArg("100");
        assertEquals(-1, value);

        value = MyParseManager.getValueFromArg("$1");
        assertEquals(1, value);
    }


    @Test
    public void testParsingMultipleArgs() {
        Integer[] values = MyParseManager.getValuesFromArgs(new String[]{"$100", "$20", "$15"});
        assertEquals(new Integer((100)), values[0]);
        assertEquals(new Integer((20)), values[1]);
        assertEquals(new Integer((15)), values[2]);

        values = MyParseManager.getValuesFromArgs(new String[]{"$100", "-$20", "$15"});
        assertEquals(null, values);

        values = MyParseManager.getValuesFromArgs(new String[]{"$100"});
        assertEquals(new Integer(100), values[0]);

        values = MyParseManager.getValuesFromArgs(new String[]{"100"});
        assertEquals(null, values);


    }

}
